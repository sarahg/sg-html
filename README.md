# sarahg.dev

My personal website. Keeping it simple this time!

## Local dev

Run a local webserver:

```shell
python3 -m http.server
```